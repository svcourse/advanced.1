<?php
/**
 * Created by PhpStorm.
 * User: vh
 * Date: 3/15/2017
 * Time: 7:07 PM
 */

$rawBody = file_get_contents("php://input");
/**
 * should be receiving:
 * name => nameValue
 */
$decodedBody = json_decode($rawBody);

$response = ['message'=> 'hello '.$decodedBody->name.' '.date("d.m.y h:i:s")];

header('Content-Type: application/json');
echo json_encode($response);

